import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

import 'screens/auth.dart';
import 'screens/main.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _controller = StreamController<String>();

  void initState() {
    _getToken();
    super.initState();
  }

  void _getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    _controller.add(token);
  }

  void _saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
    _getToken();
  }

  void _clearToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    _getToken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Расписание совещаний',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: StreamBuilder(
          stream: _controller.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData == false) {
              return AuthScreen(_saveToken);
            } else {
              final token = snapshot.data;
              return MainScreen(token, _clearToken);
            }
          }),
    );
  }
}
