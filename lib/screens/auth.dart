import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:email_validator/email_validator.dart';

import '../constants.dart';

class AuthScreen extends StatefulWidget {
  final Function setToken;

  AuthScreen(this.setToken);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _formKey = GlobalKey<FormState>();
  var isLoginMode = true;
  var isLoading = false;

  String email = '';
  String name = '';
  String password = '';
  List gender = ["Мужской", "Женский"];
  String select;

  void _switchLoginMode() {
    setState(() {
      isLoginMode = !isLoginMode;
    });
  }

  void _submitRegistration(BuildContext context) async {
    final isValid = _formKey.currentState.validate();
    if (isValid) {
      _formKey.currentState.save();
      try {
        if (isLoginMode) {
        } else {}
      } catch (e) {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Theme.of(context).errorColor,
            content: Text(e.response.toString()),
          ),
        );
      } finally {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Row addRadioButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: gender[btnValue],
          groupValue: select,
          onChanged: (value) {
            setState(() {
              print(value);
              select = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final actionText = isLoginMode ? 'Вход' : 'Регистрация';
    final switchActionText = isLoginMode
        ? 'Создать новый аккаунт'
        : 'Использовать существующий аккаунт';

    return Scaffold(
      appBar: AppBar(
        title: Text('Добро пожаловать!'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Builder(
          builder: (ctx) => Form(
            key: _formKey,
            child: Column(
              children: [
                if (!isLoginMode)
                  TextFormField(
                    key: Key('name'),
                    decoration: InputDecoration(labelText: 'Имя'),
                    onSaved: (newValue) => name = newValue,
                    validator: (value) =>
                        value.isEmpty ? 'Имя не может быть пустым' : null,
                  ),
                TextFormField(
                  key: Key('email'),
                  decoration: InputDecoration(labelText: 'Электронная почта'),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    bool isValid = EmailValidator.validate(value);
                    if (!isValid) {
                      return 'Некорректный емейл';
                    }
                    return null;
                  },
                  onSaved: (newValue) => email = newValue,
                ),
                TextFormField(
                  key: Key('age'),
                  decoration: InputDecoration(labelText: 'Возраст'),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (int.tryParse(value) <= 5) {
                      return 'Вы должны быть старше 5 лет чтоб пользоваться нашей программой!';
                    }
                    return null;
                  },
                  onSaved: null,
                ),
                Row(
                  children: <Widget>[
                    Text('Ваш пол:'),
                    addRadioButton(0, 'Мужской'),
                    addRadioButton(1, 'Женский'),
                  ],
                ),
                TextFormField(
                  key: Key('password'),
                  decoration: InputDecoration(labelText: 'Пароль'),
                  obscureText: true,
                  onSaved: (newValue) => password = newValue,
                  validator: (value) => value.length >= 6
                      ? null
                      : 'Минимальная длина пароля 6 символов',
                ),
                SizedBox(height: 20),
                if (!isLoading)
                  RaisedButton(
                    onPressed: () => _submitRegistration(ctx),
                    child: Text(actionText),
                  ),
                SizedBox(height: 5),
                if (!isLoading)
                  FlatButton(
                    onPressed: _switchLoginMode,
                    child: Text(switchActionText),
                  ),
                if (isLoading) CircularProgressIndicator()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
