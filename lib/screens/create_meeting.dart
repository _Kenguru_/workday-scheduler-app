import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

import '../constants.dart';
import '../widgets/room.dart';

class CreateMeeting extends StatefulWidget {
  final String token;
  final List<int> usedTimes;

  CreateMeeting(this.token, this.usedTimes);

  @override
  _CreateMeetingState createState() => _CreateMeetingState();
}

class _CreateMeetingState extends State<CreateMeeting> {
  static const times = ['09', '10', '11', '12', '14', '15', '16'];
  var isLoadingRooms = false;
  var rooms = [];
  String selectedTime;

  _getRooms() async {
    try {
      setState(() {
        isLoadingRooms = true;
      });
      Response response = await Dio().get(
        "${GlobalConstants.apiUrl}rooms?time=$selectedTime",
        options: Options(headers: {"Authorization": "Bearer ${widget.token}"}),
      );
      setState(() {
        rooms = response.data;
      });
    } finally {
      setState(() {
        isLoadingRooms = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Новое совещание"),
      ),
      body: Center(
        child: Column(
          children: [
            DropdownButton(
              items: times
                  .map((e) => int.parse(e))
                  .where((item) => !widget.usedTimes.contains(item))
                  .map(
                    (e) => DropdownMenuItem(
                      value: e,
                      child: Center(child: Text("$e:00")),
                    ),
                  )
                  .toList(),
              hint: Text('Выберите время совещания'),
              onChanged: (val) {
                setState(() {
                  selectedTime = val.toString().padLeft(2, '0');
                });
                if (selectedTime != null) {
                  _getRooms();
                }
              },
            ),
            SizedBox(
              height: 15,
            ),
            Text(selectedTime != null
                ? "Выбрано время $selectedTime:00"
                : 'Время не выбрано'),
            SizedBox(
              height: 15,
            ),
            if (isLoadingRooms) CircularProgressIndicator(),
            if (!isLoadingRooms)
              Expanded(
                child: ListView.builder(
                    itemBuilder: (context, index) {
                      final room = rooms[index];
                      return Room(
                        widget.token,
                        roomId: room['id'],
                        isFree: room['isFree'],
                        time: int.parse(selectedTime),
                      );
                    },
                    // separatorBuilder: null,
                    itemCount: rooms.length),
              ),
          ],
        ),
      ),
    );
  }
}
