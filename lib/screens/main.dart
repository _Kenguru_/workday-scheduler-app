import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

import '../constants.dart';
import './create_meeting.dart';
import '../widgets/invite.dart';
import './meeting.dart';

class MainScreen extends StatefulWidget {
  final String token;
  final Function clearToken;
  MainScreen(this.token, this.clearToken);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  _getMeetings() async {
    Response response = await Dio().get(
      "${GlobalConstants.apiUrl}meetings",
      options: Options(headers: {"Authorization": "Bearer ${widget.token}"}),
    );
    return response.data;
  }

  _onStatusChange(dynamic item, String status) async {
    print("Bearer ${widget.token}");
    Response response = await Dio().post(
      "${GlobalConstants.apiUrl}meetings/change_invite_status?id=${item['meeting']['id']}&inviteeId=${item['inviteeId']}&status=$status",
      options: Options(
        headers: {"Authorization": "Bearer ${widget.token}"},
        validateStatus: (status) {
          return status < 500;
        },
      ),
    );
    print(response);
    setState(() {});
  }

  _goToMeeting(int id) {
    Navigator.of(context)
        .push(
          MaterialPageRoute(
            builder: (context) => Center(
              child: MeetingScreen(widget.token, id),
            ),
          ),
        )
        .then((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getMeetings(),
      builder: (context, snapshot) {
        if (snapshot.hasData == null || snapshot.data == null) {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          final usedTimes = List<int>.from(
            snapshot.data.map((item) => item['meeting']['time']).toList(),
          );

          return Scaffold(
            appBar: AppBar(
              actions: [
                IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: widget.clearToken,
                )
              ],
              title: Text('Мои совещания'),
            ),
            body: RefreshIndicator(
              onRefresh: () async => setState(() {}),
              child: Center(
                child: snapshot.data.length == 0
                    ? Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Text(
                            'Пока нет ни одного пришлашения. Но вы можете сами пригласить коллег.'),
                      )
                    : ListView(
                        children: snapshot.data
                            .map<Widget>(
                              (item) => Invite(
                                status: item['status'],
                                room: item['meeting']['roomId'],
                                time: item['meeting']['time'],
                                id: item['meeting']['id'],
                                onTap: _goToMeeting,
                                onAccept: item['status'] == 'pending'
                                    ? () => _onStatusChange(item, 'accepted')
                                    : null,
                                onDecline: item['status'] == 'pending'
                                    ? () => _onStatusChange(item, 'declined')
                                    : null,
                              ),
                            )
                            .toList(),
                      ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.of(context)
                      .push(
                        MaterialPageRoute(
                          builder: (context) =>
                              CreateMeeting(widget.token, usedTimes),
                        ),
                      )
                      .then((_) => setState(() {}));
                }),
          );
        }
      },
    );
  }
}
