import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:mobfront/widgets/invitee.dart';

import '../constants.dart';

class MeetingScreen extends StatefulWidget {
  final String token;
  final int id;
  var isLoading = false;

  MeetingScreen(this.token, this.id);

  @override
  _MeetingScreenState createState() => _MeetingScreenState();
}

class _MeetingScreenState extends State<MeetingScreen> {
  _getMeeting() async {
    final options =
        Options(headers: {"Authorization": "Bearer ${widget.token}"});

    Future<Response> meetingFuture = Dio().get(
      "${GlobalConstants.apiUrl}meetings/${widget.id.toString()}",
      options: options,
    );

    Future<Response> usersFuture = Dio().get(
      "${GlobalConstants.apiUrl}users/",
      options: options,
    );

    List<Response> results = (await Future.wait([meetingFuture, usersFuture]));
    Map<int, dynamic> dataArrays = results.map((r) => r.data).toList().asMap();

    return dataArrays;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getMeeting(),
      builder: (context, snapshot) {
        if (snapshot.hasData == null || snapshot.data == null) {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else {
          final meeting = snapshot.data[0];
          final users = snapshot.data[1];

          return Scaffold(
            appBar: AppBar(
              title: Text('Совещание'),
            ),
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: ListView(
                  children: [
                    Center(
                      child: Text(
                        'Комната №' + meeting['roomId'].toString(),
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          'Время: ' + meeting['time'].toString() + ':00',
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(child: Text('Список участников:')),
                    SizedBox(
                      height: 10,
                    ),
                    Column(
                      children: meeting['invitations']
                          .map<Widget>(
                            (t) => Invitee(
                              status: t['status'],
                              name: t['invitee']['name'],
                              isAuthor:
                                  t['invitee']['id'] == t['inviter']['id'],
                            ),
                          )
                          .toList(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(child: Text('Пригласить коллег:')),
                    Center(
                      child: widget.isLoading
                          ? CircularProgressIndicator()
                          : DropdownButton(
                              hint: Text('Выберите сотрудника'),
                              items: users
                                  .where(
                                    (user) =>
                                        user['busyAt'].contains(meeting['time'])
                                            ? false
                                            : true,
                                  )
                                  .map<DropdownMenuItem<dynamic>>(
                                    (item) => DropdownMenuItem(
                                      child: Text(item['name']),
                                      value: item['id'],
                                    ),
                                  )
                                  .toList(),
                              onChanged: (val) async {
                                setState(() {
                                  widget.isLoading = true;
                                });
                                try {
                                  Response<dynamic> response = await Dio().post(
                                    "${GlobalConstants.apiUrl}meetings/${widget.id.toString()}?inviteeId=${val.toString()}",
                                    options: Options(headers: {
                                      "Authorization": "Bearer ${widget.token}"
                                    }),
                                  );
                                } finally {
                                  setState(() {
                                    widget.isLoading = false;
                                  });
                                }
                              }),
                    )
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }
}
