import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

import '../constants.dart';

class Invite extends StatelessWidget {
  final String status;
  final int room;
  final int time;
  final int id;
  final Function onTap;
  final Function onAccept;
  final Function onDecline;

  Invite(
      {this.status,
      this.room,
      this.time,
      this.id,
      this.onTap,
      this.onAccept,
      this.onDecline});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text(
                "Cовещание на ${time.toString()}:00 в комнате № ${room.toString()}"),
            trailing: status == 'pending'
                ? ButtonBar(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      FlatButton(
                        child: Text('Принять'),
                        color: Colors.blue,
                        onPressed: onAccept,
                      ),
                      FlatButton(
                        child: Text('Отклонить'),
                        color: Colors.redAccent,
                        onPressed: onDecline,
                      ),
                    ],
                  )
                : status == 'accepted'
                    ? Icon(
                        Icons.check_circle,
                        color: Colors.green,
                      )
                    : Icon(
                        Icons.cancel,
                        color: Colors.red,
                      ),
            onTap: () => onTap(id),
          )
        ],
      ),
    );
  }
}
