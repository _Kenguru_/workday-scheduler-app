import 'package:flutter/material.dart';

class Invitee extends StatefulWidget {
  final String name;
  final String status;
  final bool isAuthor;
  Invitee({this.name, this.status, this.isAuthor});

  @override
  _InviteeState createState() => _InviteeState();
}

class _InviteeState extends State<Invitee> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            // leading: icon,
            title: Text(widget.name),
            trailing: widget.status == 'pending'
                ? Icon(
                    Icons.hourglass_empty,
                    color: Colors.blue,
                  )
                : widget.status == 'accepted'
                    ? Icon(
                        Icons.check_circle,
                        color: Colors.green,
                      )
                    : Icon(
                        Icons.cancel,
                        color: Colors.red,
                      ),
          )
        ],
      ),
    );
  }
}
