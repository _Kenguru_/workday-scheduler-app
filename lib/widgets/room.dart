import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

import '../constants.dart';
import '../screens/meeting.dart';

class Room extends StatefulWidget {
  final int time;
  final int roomId;
  final bool isFree;
  final String token;

  Room(this.token, {this.roomId, this.isFree, this.time});

  @override
  _RoomState createState() => _RoomState();
}

class _RoomState extends State<Room> {
  var isLoading = false;

  createMeeting(BuildContext context) async {
    if (isLoading) return;

    setState(() {
      isLoading = true;
    });

    try {
      Response response = await Dio().post(
        "${GlobalConstants.apiUrl}meetings",
        options: Options(headers: {"Authorization": "Bearer ${widget.token}"}),
        data: {"time": widget.time, "roomId": widget.roomId},
      );

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => Center(
            child: MeetingScreen(widget.token, response.data['id']),
          ),
        ),
      );
    } finally {
      isLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            leading: Icon(Icons.supervised_user_circle, size: 50),
            title: Text("Комната № " + widget.roomId.toString()),
            subtitle: Text(widget.isFree ? "свободна" : "занята"),
            trailing: RaisedButton(
              onPressed: widget.isFree && !isLoading
                  ? () => createMeeting(context)
                  : null,
              color: Theme.of(context).accentColor,
              textColor: Colors.white,
              child: Text('Забронировать'),
            ),
          ),
        ],
      ),
    );
  }
}
